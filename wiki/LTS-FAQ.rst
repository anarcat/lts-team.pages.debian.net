*******
LTS FAQ
*******

---------------------------------
What architectures are supported?
---------------------------------

 - Jessie supports: i386, amd64, armel and armhf.
 - Stretch supports: i386, amd64, armel, armhf and arm64.
 - Buster supports: i386, amd64, armel, armhf and arm64.

Additionally, a few packages will not be supported in the LTS version 
of the release, primarily some web-based applications which cannot be
supported for the additional timeframe. The tool ''check-support-status''
from the package `debian-security-support <https://tracker.debian.org/pkg/debian-security-support>`_
helps to detect such unsupported packages.

-----------------------------------------------------------------
What about architectures other than i386/amd64/armel/armhf/arm64?
-----------------------------------------------------------------

Those architectures are currently not supported by the Debian LTS team.
If you have computers using those architectures you should always use
the latest stable release of Debian.

The LTS team supports architectures that provide value to classes of users
interested in LTS support. Currently this means that the architecture must
have significant server deployments or must be used in big (desktop) deployments.

------------------------------------------
How much resources does the LTS team have?
------------------------------------------

The work is handled by `volunteers <https://wiki.debian.org/LTS/Team>`_
and by `paid contributors <https://wiki.debian.org/LTS/Funding>`_.
The latter rely mainly on sponsorship collected through Freexian. 
`Freexian's reports <https://raphaelhertzog.com/tag/Freexian+LTS/>`_
show how much hours are sponsored and what has been done during those hours.

-----------------------------------------------------
What is the time frame for Jessie/Stretch/Buster LTS?
-----------------------------------------------------
See dates on the `Wiki main page <https://wiki.debian.org/LTS/>`_.

---------------------------------
Who will provide updates for LTS?
---------------------------------
The release will not be handled by the Debian security team, but by a
separate group of volunteers and companies interested in making it a
success (with some overlap in people involved).
Companies using Debian who are interested in aiding this effort should
contact the mailing list `debian-lts <https://wiki.debian.org/LTS/Contact#debian-lts>`_.

---------------------------
Where can bugs be reported?
---------------------------
Please report bugs that you found in the packages to the
`debian-lts <https://wiki.debian.org/LTS/Contact#debian-lts>`_ mailing list.
The bar for severity will be raised (minor issues will no longer be fixed).

------------------------------------------------------------
Who fixes security issues with packages in jessie-backports?
------------------------------------------------------------

jessie-backports is no longer being updated, so no security issues in this suite will be fixed.

----------------------------------------------------------------------
Can unattended upgrades be used on LTS after it stops being oldstable?
----------------------------------------------------------------------
UnattendedUpgrades should work, but you should make sure that all your
installed packages are supported. If you want to know how to check if
packages get updates, see `<#what-architectures-are-supported>`_  for details.
To get more informations about updates for packages from the backports,
see the `previous question on who fixes security issues in backports <#who-fixes-security-issues-with-packages-in-jessie-backports>`_.

-----------------------------
How can I install Jessie now?
-----------------------------

See `<https://wiki.debian.org/LTS/Installing>`_.

-------------------------------------
Where are past LTS releases archived?
-------------------------------------

 - Squeeze LTS is archived at: `deb http://archive.debian.org/debian/ squeeze-lts main <http://archive.debian.org/debian/>`_
 - Wheezy LTS is archived at: `deb http://archive.debian.org/debian-security/ wheezy/updates main <http://archive.debian.org/debian-security/>`_.

See also `debian-lts thread <https://lists.debian.org/debian-lts/2020/03/msg00066.html>`_.

There are also further updates through `ELTS <https://wiki.debian.org/LTS/Extended>`_.

--------------------------------------------------
I have another question about LTS. Whom can I ask?
--------------------------------------------------
Besides this website, you can join the mailing list `debian-lts <https://wiki.debian.org/LTS/Contact#debian-lts>`_ or join `#debian-lts in the IRC <https://wiki.debian.org/LTS/Contact#IRC_Channel_.23debian-lts>`_.

