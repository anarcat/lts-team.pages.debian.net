##############
LTS Testsuites
##############

Here are some tips on how to run some non-trivial package test suites.

-------
Stretch
-------

 * `autopkgtest <https://wiki.debian.org/LTS/TestSuites/autopkgtest>`_: running/creating/backporting DEB-8 tests

 * `curl <https://wiki.debian.org/LTS/TestSuites/curl>`_

 * `ffmpeg <https://wiki.debian.org/LTS/TestSuites/ffmpeg>`_
 * `firebird* <https://lists.debian.org/debian-lts/2018/04/msg00090.html>`_

 * `ghostscript <https://lists.debian.org/debian-lts/2019/03/msg00122.html>`_ (no publicly available extensive test suite)
 * `golang <https://wiki.debian.org/LTS/TestSuites/golang>`_

 * `horde <https://wiki.debian.org/LTS/TestSuites/horde>`_

 * `jetty <https://wiki.debian.org/LTS/TestSuites/jetty>`_

 * `libxstream-java <https://wiki.debian.org/LTS/TestSuites/libxstream-java>`_

 * `mysql-connector-java <https://wiki.debian.org/LTS/TestSuites/mysql-connector-java>`_

 * `nginx <https://wiki.debian.org/LTS/TestSuites/nginx>`_

 * `openexr <https://wiki.debian.org/LTS/TestSuites/openexr>`_

 * `php <https://wiki.debian.org/LTS/TestSuites/php>`_
 * `python <https://wiki.debian.org/LTS/TestSuites/python>`_

 * `qemu <https://wiki.debian.org/LTS/TestSuites/qemu>`_

 * `rails <https://wiki.debian.org/LTS/TestSuites/rails>`_
 * `request-tracker4 <https://wiki.debian.org/LTS/TestSuites/request-tracker4>`_

 * `salt <https://wiki.debian.org/LTS/TestSuites/salt>`_
 * `sane-backends <https://wiki.debian.org/LTS/TestSuites/sane-backends>`_
 * `subversion <https://wiki.debian.org/LTS/TestSuites/subversion>`_

 * `tiff <https://wiki.debian.org/LTS/TestSuites/tiff>`_
 * `twisted <https://wiki.debian.org/LTS/TestSuites/twisted>`_

 * `uswgi <https://wiki.debian.org/LTS/TestSuites/uwsgi>`_

 * `wordpress <https://wiki.debian.org/LTS/TestSuites/wordpress>`_

 * `zabbix <https://wiki.debian.org/LTS/TestSuites/zabbix>`_

------
Jessie
------

 * `aspell <https://wiki.debian.org/LTS/TestSuites/aspell>`_

 * `clamav <https://lists.debian.org/debian-lts/2019/04/msg00117.html>`_

 * `kdepim <https://wiki.debian.org/LTS/TestSuites/kdepim>`_

 * `libav <https://wiki.debian.org/LTS/TestSuites/libav>`_
 * `libonig <https://wiki.debian.org/LTS/TestSuites/libonig>`_

 * `mariadb <https://lists.debian.org/debian-lts/2019/07/msg00049.html>`_

 * `netty <https://wiki.debian.org/LTS/TestSuites/netty>`_
 * `ntp <https://wiki.debian.org/LTS/TestSuites/ntp>`_

 * `phpmyadmin <https://wiki.debian.org/LTS/TestSuites/phpmyadmin>`_

 * `slurm-llnl <https://wiki.debian.org/LTS/TestSuites/slurm-llnl>`_
 * `sqlalchemy <https://lists.debian.org/debian-lts/2019/03/msg00063.html>`_
 * `sqlite <https://wiki.debian.org/LTS/TestSuites/sqlite>`_
 * `symfony <https://lists.debian.org/debian-lts/2019/03/msg00024.html>`_

 * `tomcat <https://wiki.debian.org/LTS/TestSuites/tomcat>`_

