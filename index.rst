.. Debian LTS team documentation master file, created by
   sphinx-quickstart on Wed May 11 23:12:34 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Debian LTS team's documentation!
===========================================

This is the public information about LTS projects and
development way.

The source of this document is at
https://salsa.debian.org/lts-team/lts-team.pages.debian.net

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   git-workflow-lts.rst
   howtos/index.rst
   wiki/index.rst
