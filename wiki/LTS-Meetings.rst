############
LTS Meetings
############

This page collects details of meetings of the `LTS <https://wiki.debian.org/LTS>`_ team. 

-----------------
Upcoming Meetings
-----------------

 * **Fourth Thursday of the month, 14 UTC**
 * The agenda is prepared at https://pad.riseup.net/p/lts-meeting-agenda
 * 2022


  * Thursday,  **July 28th**, 14 UTC on #debian-lts on irc.debian.org
  * Thursday, **August 25th**, 14 UTC on https://jitsi.debian.social/LTS-monthly-meeting
  * Thursday,  **September 29th**, 14 UTC on #debian-lts on irc.debian.org
  * Thursday, **October 27th**, 14 UTC on https://jitsi.debian.social/LTS-monthly-meeting
  * Thursday,  **November 24th**, 14 UTC on #debian-lts on irc.debian.org
  * Thursday, **December 22th**, 14 UTC on https://jitsi.debian.social/LTS-monthly-meeting

-------------
Past Meetings
-------------

NB: We use `MeetBot <https://wiki.debian.org/MeetBot>`_ for our IRC meetings. You can reach the full meeting log from the summary.

 * 2022

  * `June 23th: Jitsi meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
  * May 26th, 14 UTC on #debian-lts on irc.debian.org, cancelled
  * `April 28th: Jitsi meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
  * `March 24th: summary <http://meetbot.debian.net/debian-lts/2022/debian-lts.2022-03-24-14.00.txt>`_
  * `February 24th: Jitsi meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
  * `January 27th: summary <http://meetbot.debian.net/debian-lts/2022/debian-lts.2022-01-27-14.00.txt>`_

 * 2021

  * `MeetBot log archive for 2021 <http://meetbot.debian.net/debian-lts/2021/>`_
  * `December: Video meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
  * `November: summary <http://meetbot.debian.net/debian-lts/2021/debian-lts.2021-11-25-14.00.html>`_
  * `October: Video meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
  * `September: summary <http://meetbot.debian.net/debian-lts/2021/debian-lts.2021-09-30-14.01.html>`_
  * `August: Video meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_

   * `DebConf21 BoF <https://debconf21.debconf.org/talks/103-funding-projects-to-improve-debian/>`_

  * `July: summary <http://meetbot.debian.net/debian-lts/2021/debian-lts.2021-07-29-14.09.html>`_
  * June: No data
  * `May: Cancelled, summary <http://meetbot.debian.net/debian-lts/2021/debian-lts.2021-05-27-15.12.html>`_
  * `April: Video meeting notes <https://wiki.debian.org/LTS/Meetings?action=AttachFile&do=view&target=lts_meeting_April2021.txt>`_
  * `March: summary <http://meetbot.debian.net/debian-lts/2021/debian-lts.2021-03-25-14.58.html>`_
  * `February: Video meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
  * `January: summary <http://meetbot.debian.net/debian-lts/2021/debian-lts.2021-01-28-14.58.html>`_

 * 2020

  * `MeetBot log archive for 2020 <http://meetbot.debian.net/debian-lts/2020/>`_
  * DebConf20 BoF `talk <https://debconf20.debconf.org/talks/72-debian-lts-bof/>`_
  * DebConf20 BoF `videconf <https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/72-debian-lts-bof.webm>`_

 * 2016

  * `DebConf 16 BoF <https://lists.debian.org/debian-lts/2016/07/msg00173.html>`_

 * Debian Security Team Meeting Feb 2014 in Essen/Germany

  * `Bits from the Security Team <https://lists.debian.org/debian-devel-announce/2014/03/msg00004.html>`_ LTS notes are near the end of the text
  * `Live notes from the meeting <https://wiki.debian.org/LTS/Meetings?action=AttachFile&do=view&target=secteamessen2014.html>`_ notes about LTS are near the end

