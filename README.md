Debian LTS team's documentation.

The repo creates the <https://lts-team.pages.debian.net> website. It is based
on Sphinx and regenerates after each commit into the master.

The following packages need to be installed:

* python3-sphinx      (to get `sphinx-build`)
* python3-myst-parser (to get `myst_parser`, which is needed by sphinx-build)
* sensible-utils      (to get `sensible-browser`)

The package `python3-myst-parser` is only available since Bookworm.

Local build can be done as follows:

	sphinx-build -b html . public

After that generated files are placed in `./public-directory`, and can be
viewed with:

	sensible-browser public/index.html

For useful tips on how to organize documentation, see
<https://documentation.divio.com/>
