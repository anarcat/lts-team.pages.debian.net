
Git workflow for the (E)LTS packages
====================================

Rationale
---------

(E)LTS team manages several releases and many packages. To keep the history of all uploads the git-workflow is mandatory.
All packages are hosted in `this repository <https://salsa.debian.org/lts-team/packages/>`_.

For each package we have a dedicated repository. Even if the package has already its own
repository for the normal maintenance work, we create our own one in order to keep the (E)LTS-specific
upload to avoid any conflicts with the original maintainer(s).

Each package must have a working CI (continuous integration) pipeline. An exception can be granted to some large or difficult
packages where the CI is difficult to setup or is impossible for some technical reason. Each
exception should be discussed on the `LTS mailing list <https://lists.debian.org/debian-lts>`_.

This document describes the steps needed for the new repository creation, setting up a CI system
and preparation of an upload.

Prepare a new repository
------------------------

.. note::
   Please check whether an original package is using git-workflow/Salsa-CI already. If so, it is preferred to fork the maintainer's existing repo to reduce resource consumption on Salsa.


As an example, the package ``admesh`` for the LTS upload into ``debian/stretch`` will be used. You will of course have to use your own package name and corresponding release.


#. First check whether the repository for this package exists. If yes; skip these steps and go to the *Prepare an upload...* section.
#. Go to the `main repo <https://salsa.debian.org/lts-team/packages>`_ and press "New project" -> "`Create blank project <https://salsa.debian.org/projects/new?namespace_id=6008#blank_project>`_".
#. For the "Project name" field, enter ``admesh``.
#. Press "Create project".

We are using the `DEP-14 <https://dep-team.pages.debian.net/deps/dep14/>`_ schema, where the branches are named according to the releases, such as ``debian/stretch``\ , ``debian/jessie``\ , etc.

We drop the ``main`` branch and create corresponding branches.


#. Go to "Repository->branches" and press "New branch".
#. Branch name -> ``debian/stretch`` or any other suite, which is needed for your work now.
#. Go to  "Settings->Repository->Default Branch" and choose ``debian/stretch``->"Save changes".
#. Go to "Repository->Branches" and remove ``main`` branch.

Setting up a CI
^^^^^^^^^^^^^^^


#. Go to "Settings->CI/CD". Expand "General pipelines".:

   #. Enter in "CI/CD configuration file": **debian/.gitlab-ci.yml**
   #. Enter in 'Timeout" **6h**.
   #. Press "Save changes".

Import of new version of the package
------------------------------------

Assume, you have a folder "orig" where the current version of the package is download and uncompressed.

.. code-block::

   ls -l ../orig/

   drwxr-xr-x    - anton 16 May 22:36 admesh-0.98.2
   .rw-r--r-- 5.6k anton 16 May 22:36 admesh_0.98.2-3.debian.tar.xz
   .rw-r--r-- 2.1k anton 16 May 22:36 admesh_0.98.2-3.dsc
   .rw-r--r--  34k anton 16 May 22:36 admesh_0.98.2.orig.tar.xz




#. 
   In the terminal locally clone firstly an empty Repository.

   .. code-block::

       git clone git@salsa.debian.org:lts-team/packages/admesh.git
       cd admesh
       rm README.md
       git commit -am'Remove README.md'

#. 
   Create upstream branch and import the source:

   .. code-block::

       git checkout -b upstream
       gbp import-orig --debian-branch=debian/stretch --pristine-tar  ../orig/admesh_0.98.2.orig.tar.xz

#. 
   Import initial debian-folder:

   .. code-block::

       git checkout debian/stretch
       cp -r ../orig/admesh-0.98.2/debian .
       git add debian
       git commit -am'Initial import of debian/ folder'

#. 
   Delete ``debian/gbp.conf`` if it exists. We do this in a separate commit to record it in Git history.

   .. code-block::

       rm debian/gbp.conf && git commit -am 'Remove debian/gbp.conf'

#. 
   Tag the initial upload:

   .. code-block::

       gbp buildpackage --git-tag-only --git-sign-tags --git-debian-branch=debian/stretch

Add gitlab-ci.yml
^^^^^^^^^^^^^^^^^


#. 
   Create the file **debian/.gitlab-ci.yml** with the following content:

   .. code-block::

       include:
       - https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/recipes/debian.yml

       variables:
           RELEASE: 'stretch'
           SALSA_CI_COMPONENTS: 'main contrib non-free'
           SALSA_CI_DISABLE_REPROTEST: 1
           SALSA_CI_DISABLE_LINTIAN: 1


   Alternatively file can be downloaded:

   .. code-block::

       wget https://salsa.debian.org/lts-team/packages/miscellaneous/-/raw/main/stretch/.gitlab-ci.yml -P debian

   The full documentation for the Debian pipeline `may be found on the salsa-ci-team/pipeline repository <https://salsa.debian.org/salsa-ci-team/pipeline>`_.

#. 
   Commit and push the file:

   .. code-block::

       git add debian/.gitlab-ci.yml
       git commit -am'Add debian/.gitlab-ci.yml'
       git push --all
       git push --tags

If all went well, you can go to the salsa on the `package page <https://salsa.debian.org/lts-team/packages/admesh/-/pipelines>`_\ , section "CI/CD->Pipelines" and check whether the package is being built successfully or not.


.. image:: /_static/git-workflow-lts/CI-successfull.png
   :target: /_static/git-workflow-lts/CI-successfull.png
   :alt: A test image


As you see in our ``.gitlab-ci.yml`` file, we disable lintian and reprotest jobs because they are less relevant for the minimal security updates. Those options can be `switched on explicitly <https://salsa.debian.org/lts-team/packages/admesh/-/commit/386a0219508d5cd50f32fc33922a1d63934f2b4f>`_. Lintian is also disabled due to the fact that the newer version is used in for older releases and it sometimes produces some errors.

Prepare an upload of the already existing package
-------------------------------------------------


#. 
   Clone an existing repository:

   .. code-block::

       git clone git@salsa.debian.org:lts-team/packages/admesh.git

#. 
   Hack on the package

#. 
   Try to build locally:

   .. code-block::

       gbp buildpackage --git-debian-branch=debian/stretch

#. 
   After ``git push`` you can check the `status of the package <https://salsa.debian.org/lts-team/packages/admesh/-/pipelines>`_

#. 
   If the build was successful, the package should be tagged:

   .. code-block::

       gbp buildpackage --git-tag-only --git-sign-tags --git-debian-branch=debian/stretch 
       git push --tags
