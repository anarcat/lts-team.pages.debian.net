==================
Tag issues for LTS
==================

Information is tracked in ``data/CVE/list`` in the `LTS security tracker`_.

The meaning of tags is in https://wiki.debian.org/LTS/Development under **CVE
triaging in the LTS release**.

.. _`LTS security tracker`: https://salsa.debian.org/security-tracker-team/security-tracker

